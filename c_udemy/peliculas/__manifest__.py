# -*- coding:utf-8 -*-
{
    'name':'Modulo de peliculas',
    'version': '1.0',
    'depends': [
        'contacts'
    ],
    'author': 'Carlos Uitz',
    'category': 'Peliculas',
    'description': ''''
    Modulo de presupuestos de peliculas
    ''',
    'data': [
        'data/categoria.xml',
        'data/secuencia.xml',
        'views/menu.xml',
        'views/presupuesto_views.xml',

    ],
}