# -*- coding:utf-8 -*-
import logging
from odoo import fields, models, api
from odoo.exceptions import UserError
logger = logging.getLogger(__name__)

class Presupuesto(models.Model):
    _name = "presupesto"

    name = fields.Char(string='Pelicula')
    clasificacion = fields.Selection(selection=[
        ('G', 'G'),
        ('PG', 'PG'),
        ('PG-13', 'PG-13'),
        ('R', 'R'),
        ('NC-17', 'NC-17'),

    ], string='Clasificacion ')
    dsc_clasificacion =fields.Char(string='Descripcion de la clasificacion')
    fch_estreno = fields.Date(string='Fecha estreno')
    puntuacion = fields.Integer(String='Puntuacion', related="puntuacion2")
    puntuacion2 = fields.Integer(String='Puntuacion2')
    active = fields.Boolean(string='Activo', default=True)
    director = fields.Many2one(
        comodel_name='res.partner',
        string='Director'
    )
    categoria_director_id= fields.Many2one(
        comodel_name='res.partner.category',
        string='Categoria Director',
        default=lambda self: self.env.ref('peliculas.category_director')
        #primera version
        #default=lambda self: self.env['res.partner.category'].search([('name', '=', 'Director')])
    )
    genero_ids = fields.Many2many(
        comodel_name='genero',
        string='Genero'
    )
    vista_general = fields.Text(string='Descripcion')
    link_trailer = fields.Char(string='Trailer')
    es_libro = fields.Boolean(string='Version Libro')
    libro = fields.Binary(string='Libro')
    libro_filename = fields.Char(string='Nombre del libro')

    state = fields.Selection(selection=[
        ('borrador', 'Borrador'),
        ('aprobado', 'Aprobado'),
        ('cancelado', 'Cancelado'),
    ], default='borrador', string='Estados', copy=False)
    fch_aprobado= fields.Datetime(string='Fecha aprobado', copy=False)
    num_presupuesto = fields.Char(string='Numero de presupuesto', copy=False)
    fhc_creacion = fields.Datetime(string='Fecha creacion', copy=False, default=lambda self: fields.Datetime.now())
    act_ids = fields.Many2many(
        comodel_name='res.partner',
        string='Actores'
    )
    categoria_actor_id = fields.Many2one(
        comodel_name='res.partner.category',
        string='Categoria Actor',
        default=lambda self: self.env.ref('peliculas.category_actor')
    )
    opinion = fields.Html(string='Opinion')
    detalle_ids= fields.One2many(
        comodel_name='presupuesto.detalle',
        inverse_name='presupuesto_id',
        string='Detalles'
    )

    def aprobar_presupuesto(self):
        self.state = 'aprobado'
        self.fch_aprobado = fields.Datetime.now()


    def cancelar_presupuesto(self):
        self.state = 'cancelado'

    def unlink(self):
        for record in self:
            if record.state !='cancelado':
                raise UserError('No se puede eliminar el registro por que su estado no es cancelado')
            super(Presupuesto, record).unlink()
    @api.model
    def create(self, variables):
        logger.info('variables: {0}'.format(variables))
        sequence_obj = self.env['ir.sequence']
        correlativo = sequence_obj.next_by_code('secuencia.presupuesto.pelicula')
        variables['num_presupuesto'] = correlativo
        return super(Presupuesto, self).create(variables)


    @api.onchange('clasificacion')
    def _onchange_clasificacion(self):
        if self.clasificacion:
            if self.clasificacion == 'G':
                self.dsc_clasificacion ='Publico general'
            if self.clasificacion == 'PG':
                self.dsc_clasificacion ='Se recomienda compañia de un adulto'
            if self.clasificacion == 'PG-13':
                self.dsc_clasificacion ='Mayores de 13'
            if self.clasificacion == 'R':
                self.dsc_clasificacion = 'En compañia de un adulto obligatorio'
            if self.clasificacion == 'NC-17':
                self.dsc_clasificacion ='Mayores de 18'
        else:
            self.dsc_clasificacion=False

class PresupuestoDetalle(models.Model):
    _name = "presupuesto.detalle"

    presupuesto_id = fields.Many2one(
        comodel_name='presupesto',
        string='Presupuesto'
    )

    name = fields.Many2one(
        comodel_name='recurso.cinematografico',
        string='Recurso'
    )

    descripcion = fields.Char(string='Descripcion', related='name.descripcion')
    contacto_id = fields.Many2one(
        comodel='res.partner',
        string='Contacto',
        related='name.contacto_id'
    )
    imagen =fields.Binary(string='Imagen',related='name.imagen')
    cantidad =fields.Float(string='Cantidad', default=1.0, digits=(16, 4))
    precio =fields.Float(string='precio')
    importe =fields.Float(string='importe')

    @api.onchange('name')
    def _onchange_name(self):
        if self.name:
            self.precio = self.name.precio

    @api.onchange('cantidad', 'precio')
    def _onchange_importe(self):
        self.importe =self.cantidad * self.precio
